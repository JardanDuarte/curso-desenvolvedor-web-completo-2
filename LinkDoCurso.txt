Entidades HTML (Caracteres especiais)
http://www.w3schools.com/html/html_entities.asp

Tipos de bordas:
http://www.w3schools.com/css/css_border.asp,

Códigos de cores html Online:
http://html-color-codes.info/Codigos-de-Cores-HTML/

Tipos de fontes:
http://www.w3schools.com/css/css_font.asp

Fontes seguras para utilizar em seus projetos:
http://www.w3schools.com/cssref/css_websafe_fonts.asp

Link para a extensão do Chrome Web Developer:

https://chrome.google.com/webstore/detail/web-developer/bfbameneiokkgbdmiekhjnmfkcnldhhm

Normalize CSS

https://necolas.github.io/normalize.css/

Propriedade box-sizing:

http://www.w3schools.com/cssref/css3_pr_box-sizing.asp

Cantos arredondados:

http://www.w3schools.com/css/css3_borders.asp

Links Úteis
Transparências:
http://www.w3schools.com/css/css3_colors.asp

Degradês:
http://www.w3schools.com/css/css3_gradients.asp

Sombras em textos:

http://www.w3schools.com/cssref/css3_pr_text-shadow.asp

Sombras em caixas:

http://www.w3schools.com/cssref/css3_pr_box-shadow.asp

Exemplos de Grids com Bootstrap: https://getbootstrap.com/examples/grid/

Mais opções de Grids: http://getbootstrap.com/css/

Spotify - Media queries (links úteis)
Tipos de mídias:

all – todos os dispositivos
aural – sintetizadores de voz
braille – leitores de Braille
embossed – impressoras de Braille
handheld – dispositivos de mão. Por exemplo: celulares com telas pequenas.
print – impressoras convencionais
projection – apresentações de slides
screen – monitores coloridas
tty – teleimpressores e terminais
tv – televisores
Exemplo de utilização:

<link rel="stylesheet" media="print" href="print.css" /> 

Exemplos de resoluções de telas:

￼320 pixels – Smartphones no modo retrato.
480 pixels – Smartphones no modo paisagem.
600 pixels – Tablets pequenos. Ex: Amazon Kindle (600×800)
￼768 pixels – Tablets maiores em modo retrato. Ex: iPad (768×1024)
1024 pixels – Tablets maiores em modo paisagem, monitores antigos.
￼1200 pixels – Monitores wide.
Links sistema de grid:

http://getbootstrap.com/css/#grid

Exemplos de utilização media queries:

@media screen and (max-width: 767px) {
 
}
Link do recurso respond.js:

https://cdnjs.com/libraries/respond.js/

Spotify - Overflow (Links úteis)
Link de referência:

https://www.w3schools.com/cssref/pr_pos_overflow.asp

Spotify - Escondendo elementos (links úteis)
Recursos úteis para esconder conteúdos:

http://getbootstrap.com/css/#responsive-utilities